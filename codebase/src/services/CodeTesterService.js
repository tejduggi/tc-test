/**
 * Tester Service for `Code` type of Marathon Matches
 */

const fs = require("fs");
const path = require("path");
const config = require("config");

const logger = require("../common/logger");

const {
  buildDockerImage,
  deleteDockerImage,
  createContainer,
  executeSubmission,
  killContainer
} = require("../common/docker");

let submissionDirectory;
let containerId;

module.exports.performCodeTest = async (submissionId, submissionPath) => {
  try {
    submissionDirectory = path.resolve(`${submissionPath}/submission`);

    const solutionFile = path.resolve(
      `${submissionPath}/submission/solution/solution.csv`
    );

    fs.access(solutionFile, fs.constants.F_OK, err => {
      if (err) {
        logger.info("solution.csv file is not present. OK to proceed");
      } else {
        logger.info(`Deleting existing "solution.csv" file`);
        fs.unlinkSync(solutionFile);
        logger.info("File deleted, OK to proceed");
      }
    });

    // BUILD IMAGE
    await Promise.race([
      buildDockerImage(submissionDirectory, submissionId),
      new Promise((resolve, reject) => {
        setTimeout(
          () => reject("Timeout :: Docker Container Start"),
          config.TESTING_TIMEOUT
        );
      })
    ]);

    const testCommand = config.SOLUTION_COMMAND;

    // Start solution container
    containerId = await Promise.race([
      createContainer(
        submissionDirectory,
        submissionId,
        submissionId,
        config.DOCKER_MOUNT_PATH,
        testCommand
      ),
      new Promise((resolve, reject) => {
        setTimeout(
          () => reject("Timeout :: Docker Container Start"),
          config.TESTING_TIMEOUT
        );
      })
    ]);

    // Execute solution
    await Promise.race([
      executeSubmission(submissionDirectory, containerId, testCommand),
      new Promise((resolve, reject) => {
        setTimeout(
          () => reject("Timeout :: Docker Execution"),
          config.TESTING_TIMEOUT
        );
      })
    ]);

    await killContainer(containerId);

    logger.info("CODE part of execution is completed");
  } catch (error) {
    logger.error(error);
  } finally {
    await deleteDockerImage(submissionId);
    logger.info("CODE Testing cycle completed");
  }

  return;
};
