#SAMPLE DOCKERFILE TO GENERATE THE IMAGE FOR SCORER
FROM openjdk:8-jdk-alpine3.9

COPY scorer /scorer

WORKDIR /scorer

RUN ./build-detect.sh

CMD ["java", "-jar", "/scorer/tester.jar"]
